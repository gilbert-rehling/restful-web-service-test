/*
 * MyPal Pet (mypalpet.gilbert-rehling.com)
 * @version    $Id: $file.filename 10001 18/07/17 9:40 PM Gilbert Rehling $
 * @package    MyPal Pet REST Project by Gilbert Rehling (www.gilbert-rehling.com)
 * @subpackage mypalpet
 * @link       https://mypalpet.gilbert-rehling.com
 * @copyright  Copyright (c) 2017. Gilbert Rehling of MMFAW. All rights reserved. (www.mfmaw.com)
 * @license   MyPal Pet! is licensed software. No unauthorised copying or replication allowed!
 * MyPal Pet may be released as FREEWARE with a limited usage license.
 * This program is designed as a Laravel / Angular project
 * Any other use is prohibited and may infringe on the copyright and usage expectations of ths software.
 * See COPYRIGHT.php for copyright notices and further details.
 */

var mypalpet = angular.module('mypalpet', ['core', 'manage', 'ui.router', 'ngCookies', 'ngSanitize', 'ngDialog']);

mypalpet.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/list');

    $stateProvider.state('list', {
        url: '/list',
        templateUrl: '/templates/list.html',
        controller: 'ListCtrl'
    });

    $stateProvider.state('add', {
        url: '/add',
        templateUrl: '/templates/form.html',
        controller: 'AddCtrl'
    });

    $stateProvider.state('edit', {
        url: '/edit',
        templateUrl: '/templates/edit.html'
    });

    $stateProvider.state('delete', {
        url: '/delete',
        templateUrl: '/templates/delete.html',
        controller: 'DeleteCtrl'
    });

}]);

mypalpet.service('scopeService', function() {
    return {
        safeApply: function ($scope, fn) {
            var phase = $scope.$root.$$phase;
            if (phase == '$apply' || phase == '$digest') {
                if (fn && typeof fn === 'function') {
                    fn();
                }
            } else {
                $scope.$apply(fn);
            }
        }
    };
});

mypalpet.controller('MypalpetCtrl', ['$rootScope', '$scope', '$cookies', '$window', '$location', '$state', '$http', '$timeout', 'ngDialog', 'currentSpot', 'scopeService', 'USER_ROLES', 'APP_ENV', 'AuthService', 'UserService', 'AuthResolver', 'PUBLIC', 'activeMenu', 'Session', 'MypalApi', function ($rootScope, $scope, $cookies, $window, $location, $state, $http, $timeout, ngDialog, currentSpot, scopeService, USER_ROLES, APP_ENV, AuthService, Auth, AuthResolver, PUBLIC, activeMenu, Session, MypalApi) {

    /* Active View values */
    var index = 0, selectedId = -1,  rings = [];

    /* Create the Session */
    Session.create('load', 1 );

    /*Initialisation Data */
    $scope.view 	    = null;
    $scope.currentUser  = 'anonymous';
    $scope.userRoles 	= USER_ROLES;
    $scope.userRole 	= 'guest';
  //  $scope.isAuthorized = AuthService.isAuthorized;

    /* User Data */
    $scope.userID 	= 12345678;
    $scope.username = 'Anonymous';
    $scope.password = 'hello';

    /* Mypalpet View Data Containers */
    $scope.ads 			  = [];
    $scope.adsArray		  = [];
    $scope.adID			  = false;
    $scope.searchKey      = '';
    $scope.listView   	  = false;
    $scope.areaBoxDisplay = [];
    $scope.updateMessage  = null;
    $scope.errorMessage   = null;
    $scope.deleteMessage  = null;

    /* monitor the update message value */
    $scope.$watch('updateMessage', function (newValue, oldValue) {
        if ((oldValue == undefined || oldValue == false) && newValue != oldValue) {
            $timeout(function () { $scope.updateMessage = ''; }, 10000);
        }
    });

    /* populate the search scope */
    $scope.setSearch = function (event) {
        var v = angular.element("#searchKeyValue").val();
        if (v == undefined) {
            v = '';
        }
        $scope.searchKey = v;
    };

    /* load the header and footer templates into the scope */
    $timeout(function () { $scope.headerTemplate = '/templates/header.html'; $scope.footerTemplate = '/templates/footer.html'; }, 500);

    /* function to load the header-template */
    $scope.getHeader = function () {
        return $scope.headerTemplate;
    };

    /* function to load the footer */
    $scope.getFooter = function () {
        return $scope.footerTemplate;
    };

    /* set the active menu item */
    $scope.isActive = function (menuId) {
        return currentSpot.getActiveMenu() == menuId;
    };

    /* get the current page title */
    $scope.getTitle = function () {
        return currentSpot.getTitle();
    };

    /* get the current active menu */
    $scope.getActiveMenu = function () {
        return currentSpot.getActiveMenu();
    };

    /* set the loaded state */
    $scope.isSiteLoaded = function() {
        return $scope.loadedSite;
    };

    /* Mypalpet Controller Assistant Functions */
    $scope.uiRouterState = $state;

    $scope.clearView = function () {
        $("[data-ui-view]").html('');
    };

    /* begin the add process */
    $scope.startPost = function () {
        $scope.showLoader();
        $state.go('add');
    };

    /* load the lost view */
    $scope.startList = function () {
        $scope.showLoader();
        $state.go('list');
    };

    /* reopen the loader in the front-end */
    $scope.showLoader = function () {
        angular.element('.loading-message').html('<p class="top-loader text-center"><img alt="Loading please wait" src="../img/ajax-loader-small.gif" /></p>');
    };

    /* remove any forced hidden elements (sometimes templates in angular become visible before they are populated) */
    $scope.clearHidden = function () {
        $('.mypalpet-body h3, .mypalpet-body div, .mypalpet-body p, .mypalpet-body span').each (function (){
            $(this).removeClass('hide-loading');
        });
    };

    /* Common methods shared to the manage.js controllers */
    $scope.isLoading = function () {
        return $scope.isBusy(-2);
    };

    /* set the error state */
    $scope.hasError = function () {
        return $scope.errorMessage != null;
    };

    /* set the bury state */
    $scope.busy = function (id) {
        if ($scope.isBusy(id)) {
            return;
        }
        rings.push(id);
    };

    /* determine if we are waiting for a promise */
    $scope.isBusy = function (id) {
        if (angular.isDefined(id)) {
            return rings.indexOf(id) >= 0;

        } else {
            return rings.length > 0;
        }
    };

    /* complete a promise process */
    $scope.complete = function (id) {
        var idx = rings.indexOf(id);
        if (idx < 0) {
            return;
        }
        rings.splice(idx, 1);
    };

    /* resets and clears any error message */
    $scope.reset = function () {
        selectedId = -1;
        $scope.errorMessage = null;
    };

    /* cancel the form load */
    $scope.cancel = function (event) {
        event.preventDefault();
        $state.go('list');
    };

    /* function handles the data returned from the API requests */
    $scope.useBackend = function (id, task, operation) {
        $scope.busy(id);
        $scope.errorMessage = '';
        operation()
            .then(
                function (data) {
                //    for (var x in data) {
                //        $window.alert("x = " + x);
                //        $window.alert("data-x = " + data[x]);
                //        if (x == 'data') {
                //            var d = data[x];
                //            $window.alert("d = " + d);
                //            $window.alert("d = " + d.records);
                //        }
                //    }
                    if (data.data.records || data.data.record || data.data.error) {
                        $scope.setOutput(task, data.data);
                    } else if (data.data.error) {
                        $scope.setError(data);
                    }
                    $scope.complete();
                });
    };

    /* handle any errors */
    $scope.setError = function (data) {
        $scope.reset();
        $scope.complete(id);
        if (data.status == 401) {
            $scope.errorMessage = 'You are not authorised to view this content';
        } else if (angular.isDefined(data.errorInfo.reasonCode) && data.errorInfo.reasonCode == "Member-Limit-Exceeded") {
            $scope.errorMessage = 'You have exceeded the maximum amount of allowed items';
        } else {
            $scope.errorMessage = data.errorInfo.message;
        }
    };

    /* method sets output from the 'useBackend' method according to the 'task' value */
    $scope.setOutput = function (task, data) {

        var error, type, outerArr, r, adID, elm, date;
        if (task == 'setList') {

            if(angular.isDefined(data.records)) {

                $scope.errorMessage = '';
                console.log("list load data returned:" + data);

            //    outerArr = data.records;
            //    // this enables tracking the 'published state' in the views
            //    angular.forEach(outerArr, function (value, key) {
            //        this[value.id] = value.published;
            //    }, $scope.adsArray);

                $timeout( function () { $scope.ads = data.records; }, 500);

            } else if (angular.isDefined(data.error)) {
                error = data.error;
                console.log("list load error returned: " + error);
                console.log("list load error returned: " + error[0].type);
                console.log("list load error returned: " + error[1].message);

                if (error[1].message == 'empty') {
                    $scope.errorMessage = 'Empty result';
                    $timeout(function() { activeMenu.setMenu('Add'); $state.go('add'); }, 1000);
                }
            }
        }

        if (task == 'setPublish') {

            if (angular.isDefined(data.record)) {
                $scope.p = data.record;
                r = data.record, adID = $scope.adID;
                if (r.id == adID) {
                    $scope.adsArray[adID] = 1;
                    elm = angular.element(document.querySelector('#publishValue'+adID));
                    elm.html('Yes');
                    $scope.adID = '';
                }

            } else if (angular.isDefined(data.error)) {
                $scope.errorMessage = data.error;
            }

        }

        if (task == 'setUnPublish') {

            if (angular.isDefined(data.record)) {
                $scope.p = data.record;
                r = data.record, adID = $scope.adID;

                if (r.id == adID) {
                    $scope.adsArray[adID] = 0;
                    elm = angular.element(document.querySelector('#publishValue'+adID));
                    elm.html('No');
                    $scope.adID = '';

                }

            } else if (angular.isDefined(data.error)) {
                $scope.errorMessage = data.error;
            }

        }

        if (task == 'setAd') {
            if (angular.isDefined(data.record)) {
                if (data.record.updated >= 1) {
                    $scope.updateMessage = 'Pet editing completed Successful';
                    $state.go('list');
                    return;
                }

                if (data.record[0].success == 1) {
                    $scope.updateMessage = 'Pet added successfully';
                    $state.go('list');
                }

            } else {

                error = data.error, type = error.type;
                if (type == 'data' && error.message == 'missing') {
                    $scope.addScope.errorMessage = 'Error: required data missing';
                }
            }
        }

        if (task == 'setEdit') {
            if (angular.isDefined(data.record)) {

                $timeout( function () {
                    r = data.record[0];
                    $scope.editAd = [];
                    $scope.editAd.push(r);

                    for (x in r) {
                        switch(x) {
                            case 'id':
                                $scope.adID = r[x];
                                continue;
                                break;

                            case 'userId':
                                $scope.editUserId = r[x];
                                continue;
                                break;

                            case 'published':
                                $scope.publishedBox.selectedOption = { "value" : r[x] };
                                continue;
                                break;

                            case 'age':
                                $scope.ageBox = { "id": r[x] };
                                continue;
                                break;

                            case 'listDate':
                                date = Date.parse(r[x]);
                                $scope.listDate = new Date(date);
                                continue;
                                break;

                            case 'saleDate':
                                date = Date.parse(r[x]);
                                $scope.saleDate = new Date(date);
                                continue;
                                break;

                        }
                        $scope[x + 'Box'] = r[x];
                    }

                },1000);

            } else {

                error = data.error, type = error.type;
                if (type == 'data' && error.message == 'missing') {
                    $scope.addScope.errorMessage = 'Data for editing not found';

                }
            }
        }

        if (task == 'deleteAd') {
            if (angular.isDefined(data.error)) {
                $scope.errorMessage = data.error;

            } else if (angular.isDefined(data.record)) {

                var ad = Session.get('delete');

            //    for (var x in data.record){
             //       $window.alert("x: " + x);
             //       $window.alert("data[x]: " + data.record[x]);
            //    }
                if (data.record[1].id == ad.id) {
                    $scope.ads[Session.get('deleteIndex')] = null;
                    $scope.deleteMessage = 'Ad listing with ID: ' + ad.id + ' has been deleted successfully';
                    setTimeout(function() { $state.go('list'); }, 5000);

                } else {
                    $scope.errorMessage = 'No result returned for ' + ad.id + ':' + data.record[1].id + ' - please confirm the deletion by viewing the List page'
                }
            }
        }
    };

    $scope.addScope   = false;
    $scope.curentFormValues = {};

    /* Date Picker Variables and Functions */
    $scope.today = function() {
        $scope.listDate = new Date();
        $scope.listDate.setDate($scope.listDate.getDate() + 1);
    };

    $scope.minDate = new Date();
    $scope.minDate.setDate($scope.minDate.getDate() - 14);
    $scope.maxDate = new Date();
    $scope.maxDate.setDate($scope.maxDate.getDate() + 21);
    $scope.clear = function () {
        $scope.listDate = null;
        $scope.saleDate = null;
    };

    $scope.inlineOptions = {
        customClass: getDayClass,
        minDate: $scope.minDate,
        showWeeks: true
    };

    $scope.dateOptions = {
        //  dateDisabled: disabled,
        formatYear: 'yy',
        maxDate: $scope.maxDate,
        minDate: new Date(),
        startingDay: 1
    };

    // Disable weekend selection
    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }

    $scope.toggleMin = function () {
        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;

    };

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.setDate = function (year, month, day) {
        $scope.listDate = new Date(year, month, day);
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.popup1 = {
        opened: false
    };

    $scope.popup2 = {
        opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date(tomorrow);
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    $scope.events = [{
            date: tomorrow,
            status: 'full'
        },
        {
            date: afterTomorrow,
            status: 'partially'
        }];

    function getDayClass(data) {
        var date = data.date,
            mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0,0,0,0);
            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);
                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }
        return '';
    }
    /* end date picker */

    /* MypalPet Add and Edit Functions */
    /* in case we have issues using the native Angular method we can populate the age selector using ng-repeat */
    $scope.ages = [];
    $scope.setAges = function() {
        var ages = [], i;
        for(i = 1; i <= 240; i++) {
            ages.push(i);
        }
        $scope.ages = ages;
    };

    $timeout(function() {
        $scope.today();
        $scope.toggleMin();
        $scope.setAges();
    }, 500);

    /* populates the Ages selector - use for age selection drop-down selector */
    $scope.getAgesBox = function () {
        $scope.chooseAgesBox = [];
        //$scope.chooseAgesBox.push( { id: null, age: 'Please choose the pets age' });
        for(var i = 1; i <= 240; i++) {
            $scope.chooseAgesBox.push( { id: i, age: i } );
        }
    };

    /* currently not being used */
    $scope.getPublished = function () {
        $scope.publishedBox = {availableOptions:[{value: '0', text:'Unpublished'},{value: '1', text:'Published'}], "selectedOption": {value: '1', text:'Published'}};
    };

    /* load the template to confirm deletion - used for Video deletion confirmation */
    $scope.confirmDelete = function(context, title, id) {

        if (context && title && id >= 0) {
            $scope.deleteTitle 	 = title;
            $scope.deleteContext = context;
            $scope.deleteId      = id;
            ngDialog.open({
                template: '/templates/confirm-delete-template.html',
                className: 'ngdialog-theme-plain video-edit',
                scope: $scope
            });
        }
    };

}]);


