/*
 * MyPal Pet (mypalpet.gilbert-rehling.com)
 * @version    $Id: $file.filename 10001 18/07/17 9:40 PM Gilbert Rehling $
 * @package    MyPal Pet REST Project by Gilbert Rehling (www.gilbert-rehling.com)
 * @subpackage mypalpet
 * @link       https://mypalpet.gilbert-rehling.com
 * @copyright  Copyright (c) 2017. Gilbert Rehling of MMFAW. All rights reserved. (www.mfmaw.com)
 * @license   MyPal Pet! is licensed software. No unauthorised copying or replication allowed!
 * MyPal Pet may be released as FREEWARE with a limited usage license.
 * This program is designed as a Laravel / Angular project
 * Any other use is prohibited and may infringe on the copyright and usage expectations of ths software.
 * See COPYRIGHT.php for copyright notices and further details.
 */

angular.module('core', ['ngCookies','ngDialog'])
    .constant('APP_ENV', 'MyPalPet-Development')
    .constant('apiUrl', window.location.protocol + '//' + window.location.hostname + '/api/mypalpet')
    .constant('USER_ROLES', {
        all: '*',
        admin: 'admin',
        member: 'member',
        guest: 'guest'
    })
    .constant('PUBLIC', { key: 'a1b2c3d4', secret: 'z0y9a1b2c3d4x8w7k' })
    .factory('currentSpot', ['activeMenu', '$window', function (activeMenu, $window) {
        var activeMenuId = '';
        var titleText    = '';
        return {
            setCurrentSpot: function (menuId, title) {
                activeMenuId = menuId;
                titleText    = title;
                activeMenu.setMenu(menuId);
            },
            getActiveMenu: function () {
                return activeMenuId;
            },
            getTitle: function () {
                return titleText;
            },
            setTitle: function (title) {
                titleText = title;
            }
        }
    }])
    .factory('activeMenu', function ($timeout) {
        return {
            setMenu: function (id) {
                var arr = ['List','View','Add'];
                for (var i = 0; i < arr.length; i=i+1) {
                    angular.element('#'+arr[i]).removeClass('active');
                }
                if (id == 'List') {
                    $timeout(function() { angular.element('#'+id).addClass('active'); }, 1000);
                } else {
                    angular.element('#'+id).addClass('active');
                }
            }
        }
    })
    .directive('mbActiveMenu', ['currentSpot', '$window', '$timeout', function (currentSpot, $window, $timeout) {
        return function (scope, element, attrs) {
            var activeMenuId = attrs["mbActiveMenu"], activeTitle = attrs["mbActiveTitle"], activeTitleText;
            activeTitleText = angular.element('#' + activeTitle).html();
            currentSpot.setCurrentSpot(activeMenuId, activeTitleText);
            $timeout(function() { angular.element('.loading-message').html(''); }, 1500);
        }
    }])
    .directive('mbMenuId', ['$window', 'currentSpot', function ( $window, currentSpot) {
        var menuElements = [];
        function setActive (element, menuId) {
            if (currentSpot.getActiveMenu() == menuId) {
                element.addClass('active');
            } else {
                element.removeClass('active');
            }
        }
        return function (scope, element, attrs) {
            var menuId = attrs["mbMenuId"];
            menuElements.push({ id: menuId, node: element });
            setActive(element, menuId);
            var watcherFn = function (watchScope) {
                return watchScope.$eval('getActiveMenu()');
            }
            scope.$watch(watcherFn, function (newValue, oldValue) {
                for (var i = 0; i < menuElements.length; i++) {
                    var menuElement = menuElements[i];
                    setActive(menuElement.node, menuElement.id);
                }
            });
        }
    }])
    .factory('AuthService', ['$http', '$cookies', '$window', 'Session', 'apiUrl', 'PUBLIC',  function ($http, $cookies, $window, Session, apiUrl, PUBLIC) {
        var authService = {};
        authService.login = function (credentials) {
            var url = apiUrl + '/runLogin';
            return authService.request('POST', url, credentials)
                .then(function (result) {
                    var results = result.data, data, user;
                    data = results.record;
                    user = results.user;
                    $cookies.put('MyRole', user[1].role);   // !! Update this later when live !!
                    Session.create(data.id, user[0].id, user[1].role, user[2].username, user[4].UserSecret, user[3].UserId, user[5].name);
                    return user;
                });
        };
        authService.logout = function (uid) {
            if (uid) {
                var url = apiUrl + '/runLogout/'+uid, vars = {logout: uid};
                return authService.request('POST', url, vars)
                    .then(function (result) {
                        var results = result.data, data;
                        data        = results.record;
                        $cookies.put('MyRole','');   // !! Update this later when live !!
                        $cookies.put('AdListID', '');
                        Session.destroy();
                        return data;
                    });
            } else {
                $window.alert("error - uid = "+uid);
            }
        };
        authService.reValidate = function (id) {
            var url = apiUrl + '/validate/checkLogin/'+id, cred = {key: id};
            //return $http.post(url, cred)
            return authService.request('GET', url, cred)
                .then(function (result) {
                    var results = result.data, data, user;
                    data = results.record;
                    user = results.user;
                    $cookies.put('AdListID', data.id);   // !! Update / remove this later when live !!
                    Session.create(data.id, user[0].id, user[1].role, user[2].username, user[4].UserSecret, user[3].UserId, user[5].name);
                    return user;
                });
        };
        authService.postRegistration = function (id) {
            var url = apiUrl + '/validate/postRegistration/'+id, cred = {key: id};
            //return $http.post(url, cred)
            return authService.request('GET', url, cred)
                .then(function (result) {
                    var results = result.data, data, user;
                    data = results.record;
                    user = results.user;
                    $cookies.put('AdListID', data.id);   // !! Update / remove this later when live !!
                    Session.create(data.id, user[0].id, user[1].role, user[2].username, user[4].UserSecret, user[3].UserId, user[5].name);
                    return user;
                });
        };
        authService.isAuthenticated = function () {
            return !!Session.userId;
        };
        authService.isAuthorized = function (authorizedRoles) {
            if (!angular.isArray(authorizedRoles)) {
                authorizedRoles = [authorizedRoles];
            }
            return (authService.isAuthenticated() && authorizedRoles.indexOf(Session.userRole) !== -1);
        };
        /* Posts facebook login data */
        authService.facebookLogin = function (data, callback) {
            var url =apiUrl + '/facebookLogin';
            return this.request('POST', url, data)
                .then( function (result) {
                    return result.data;
                });
        };
        authService.request = function (method, url, data) {
            if (url == null || !angular.isDefined(url)) { return; }
            var req = {
                method: method,
                url: url,
                headers: {
                    'Authorization-X': authService.getAuthHeader()
                },
                data: data
            }
            return $http(req);
        };
        authService.getAuthHeader = function () {
            return "PublicSecret " + PUBLIC.key +"," + PUBLIC.secret;
        };
        return authService;
    }])
    .factory('AuthInterceptor', ['$rootScope', '$q', 'AUTH_EVENTS', function ($rootScope, $q, AUTH_EVENTS) {
        return {
            responseError: function (response) {
                /* 500 errors may occurr if the 'Token' has expired - send back to public site */
                $rootScope.$broadcast({
                    401: AUTH_EVENTS.notAuthenticated,
                    403: AUTH_EVENTS.notAuthorized,
                    404: AUTH_EVENTS.notFound,
                    419: AUTH_EVENTS.sessionTimeout,
                    440: AUTH_EVENTS.sessionTimeout,
                    500: AUTH_EVENTS.serverError
                }[response.status], response);
                return $q.reject(response);
            }
        };
    }])
    .factory('AuthResolver', ['$rootScope', '$q', '$state', '$cookies', function ($rootScope, $q, $state, $cookies) {
        return {
            resolve: function () {
                var deferred  = $q.defer();
                var unwatch   = $rootScope.$watch('currentUser', function (currentUser) {
                    if (angular.isDefined(currentUser)) {
                        if (currentUser) {
                            deferred.resolve(currentUser);
                        } else {
                            deferred.reject();
                            var lastLogin = $cookies.get('lastLogin'), isRegistered = $cookies.get('AdultMuseMember');
                            $state.go('login');
                        }
                        unwatch();
                    }
                });
                return deferred.promise;
            }
        };
    }])
    .factory('UserService', [function UserService() {
        return {
            isLogged: true,
            username: 'Anonymous'
        };
    }])
    .service('Session', function () {
        this.create = function (key, value) {
            this[key] = value;
        };
        this.save = function (key, value) {
            this[key] = value;
        };
        this.get = function (key) {
            return this[key];
        };
        this.destroy = function () {
            for (var i = 0; i < this.length; i=i+1) {
                this[i] = null;
            }
        };
    })
    .directive('stringToNumber', function() {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function (value) {
                    return '' + value;
                });
                ngModel.$formatters.push(function (value) {
                    return parseFloat(value, 10);
                });
            }
        };
    })
    /*.config(['ngDialogProvider', function (ngDialogProvider) {
        ngDialogProvider.setDefaults({
            className: 'ngdialog-theme-default',
            plain: false,
            showClose: true,
            closeByDocument: true,
            closeByEscape: true,
            appendTo: false,
            preCloseCallback: function () {
                console.log('default pre-close callback');
            }
        })
    }])*/
    .run(['$rootScope', function ($rootScope) {
        $rootScope.typeOf = function(value) {
            return typeof value;
        };
    }]);