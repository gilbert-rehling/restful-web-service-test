/*
 * MyPal Pet (mypalpet.gilbert-rehling.com)
 * @version    $Id: $file.filename 10001 18/07/17 9:40 PM Gilbert Rehling $
 * @package    MyPal Pet REST Project by Gilbert Rehling (www.gilbert-rehling.com)
 * @subpackage mypalpet
 * @link       https://mypalpet.gilbert-rehling.com
 * @copyright  Copyright (c) 2017. Gilbert Rehling of MMFAW. All rights reserved. (www.mfmaw.com)
 * @license   MyPal Pet! is licensed software. No unauthorised copying or replication allowed!
 * MyPal Pet may be released as FREEWARE with a limited usage license.
 * This program is designed as a Laravel / Angular project
 * Any other use is prohibited and may infringe on the copyright and usage expectations of ths software.
 * See COPYRIGHT.php for copyright notices and further details.
 */

var manage = angular.module('manage', ['core', 'mypalpet', 'ngDialog', 'ui.bootstrap', 'ngSanitize']);

manage.factory('MypalApi', ['$http', '$window', 'apiUrl', 'PUBLIC',  function ($http, $window, apiUrl, PUBLIC) {
    var MypalApi = {};
    //get the list or view
    function get(param, data) {
        return request("GET", param, data);
    }
    // post new form data
    function post(param, data) {
        return request("POST", param, data);
    }
    // put update data
    function put(param, data) {
        return request("PUT", param, data);
    }
    // delete data
    function del(param) {
        return request("DELETE", param, null);
    }
    // send the actual request
    function request(method, param, data) {
        if (param == null || !angular.isDefined(param)) { return; }
        var req = {
            method: method,
            url: url(param),
            headers: {
                'Authorization-X': getAuthHeader()
            },
            data: data
        }
        return $http(req);
    }
    // generate the url
    function url(param) {
        var url = apiUrl;
        if (param == null || !angular.isDefined(param)) {
            return;
        } else {
            url = url + "/" + param;
        }
        return url;
    }
    // get and set the AUTH header
    function getAuthHeader() {
        return "PublicSecret " + PUBLIC.key +"," + PUBLIC.secret;
    }

    /**
     *  Provide access to the internal functions
     *  @param p string (param)
     *  @param d array  (data)
     */
    MypalApi.doPost = function (p, d) {
        return post(p, d);
    };
    MypalApi.doGet = function (p, d) {
        return get(p, d);
    };
    MypalApi.doDelete = function (p, d) {
        return del(p, d);
    };
    MypalApi.doPut = function (p, d) {
        return put(p, d);
    };

    // this enables header generation outside of this factory
    MypalApi.getAuthHeader = function () {
        return "PublicSecret " + PUBLIC.key +"," + PUBLIC.secret;
    };
    return MypalApi;
}]);

manage.controller('ListCtrl', ['$scope', '$parse', '$timeout', '$state', '$window', '$location', 'MypalApi', 'Session', 'ngDialog', function ($scope, $parse, $timeout, $state, $window, $location, MypalApi, Session, ngDialog) {

    /* Bind to list scope */
    $scope.listScope = angular.element(document.getElementById("list-controller-scope")).scope();

    /**
     *  List View Functions
     */
    /* re-order the list */
    $scope.order = function (predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };
    /* edit selected item from list */
    $scope.startEdit = function (id) {
        $scope.showLoader();
        Session.save('editId', id);
        $state.go('edit');
    };
    /* delete selected item from list */
    $scope.startDelete = function (index) {
        var ad = $scope.ads[index];
        Session.save('delete', ad);
        Session.save('deleteIndex', index);
        $state.go('delete');
    };
    // run this automatically
    (function getListings() {

        /* only run if path is #list */
        if ($location.path() == '/list') {

            if (!angular.isDefined($scope.userID)) {
                $scope.userID = Session.userId;
            }
            $scope.loading      = true;
            $scope.errorMessage = null;
            $scope.busy(-2);
            $timeout( function () {
                /* remove any 'hidden on load' classes - in case they still exist (should have been removed in the app.js) */
                $scope.clearHidden();
                $scope.useBackend(-2, 'setList', function () { return MypalApi.doGet("pets", null ); });
                $scope.loading = false;
                $scope.$apply();
                $scope.complete();
                $scope.loadedSite   = true; // report that the site has loaded
            }, 500);
        }
    })();

    /* DiaLog Modal for 'More' links */
    $scope.showMoreAd = function(id) {
        if (id) {
            $scope.dataLoading  = true;
            $scope.loadingData  = true;
            $scope.countries    = Session.countries;
            MypalApi.doGet("pet/" + id, null ).
            then( function (data) {
                $scope.ad     = data.data.record;
                ngDialog.open({
                    templateUrl: 'templates/more-template.html',
                    className: 'ngdialog-theme-plain',
                    scope: $scope
                });
                console.log("dialog data: " + data);
                $timeout(function() { $scope.dataLoading = false; $scope.loadingData = false; }, 500);
            });
        }
    };

    // clear any remaining system message
    if ($scope.updateMessage != null) {
        $timeout(function () { $scope.updateMessage = null; }, 10000);
    }
    if ($scope.deleteMessage != null) {
        $timeout(function () { $scope.deleteMessage = null; }, 10000);
    }

}]);

manage.controller('AddCtrl', ['$scope', '$state', 'MypalApi', '$window', function ($scope, $state, MypalApi, $window) {

    $scope.loadedSite   = true; // report that the site has loaded

    /* handle error messages */
    if ($scope.errorMessage == null) {
        console.log("no error message detected on add controller load");
        $scope.errorMessage = null;
    }

    /* pet fields */
    $scope.breedBox    = '';
    $scope.ageBox      = '';
    $scope.nameBox     = '';
    $scope.priceBox    = '';
    $scope.listDate = '';
    $scope.saleDate = '';
    $scope.required = ['breed','age','price','listDate'];
    $scope.clearHidden();
    $scope.getAgesBox();

    $scope.addAd = function () {
        var breed = $scope.breedBox.replace(/\,/g, "&#44;"), age = $scope.ageBox.id, name = $scope.nameBox.replace(/\,/g, "&#44;"), price = $scope.priceBox, listDate = $scope.listDate, saleDate = $scope.saleDate, data = false;
        data = {"breed": breed, "age": age, "name": name, "price": price, "listDate": listDate, "saleDate": saleDate };
        $scope.useBackend(-2, 'setAd', function () { return MypalApi.doPost("pet", JSON.stringify(data) ); });
    };

    /* handle update messages */
    if ($scope.updateMessage != null) {
        $timeout(function() { $scope.updateMessage = null; }, 10000);
    }

    /* cancel the form load */
    $scope.cancel = function (event) {
        event.preventDefault();
        $state.go('list');
    };
}]);

manage.controller('EditCtrl', ['$scope', '$filter', '$state', '$window', '$location', '$timeout', 'Session', 'MypalApi', function ($scope, $filter, $state, $window, $location, $timeout, Session, MypalApi) {

    /* Default values */
    $scope.errorMessage = '';
    $scope.loadingData  = true;

    (function () {
        /* because this auto-loads ensure this only runs if path is #edit */
        if ($location.path() == '/edit') {
            $scope.getAgesBox();
            $scope.errorMessage = '';
            $scope.busy(-2);
            var id = Session['editId'];
            setTimeout( function () {
                $scope.clearHidden();
                $scope.useBackend(-2, 'setEdit', function () { return MypalApi.doGet("pet/" + id, false ); });
                $scope.loadingData = false;
                $scope.complete();
            }, 500);
        }
    })();

    /* handle the update */
    $scope.updateAd = function(event) {
        var id = Session.get('editId'), breed = $scope.breedBox.replace(/\,/g, "&#44;"), age = $scope.ageBox.id, name = $scope.nameBox.replace(/\,/g, "&#44;"), price = $scope.priceBox, listDate = $scope.listDate, saleDate = $scope.saleDate, data = false;
        data = {"breed": breed, "age": age, "name": name, "price": price, "listDate": listDate, "saleDate": saleDate };
        if (id && breed && age && price && listDate) {
            $scope.useBackend(-2, 'setAd', function () { return MypalApi.doPut("pet/" + id, JSON.stringify(data) ); });
        } else {
            event.preventDefault();
            $scope.errorMessage = 'Required Data Missing';
        }
    };

    /* handle update messages */
    if ($scope.updateMessage != null) {
        $timeout(function() { $scope.updateMessage = null; }, 10000);
    }

    /* cancel the form load */
    $scope.cancel = function (event) {
        event.preventDefault();
        $state.go('list');
    };
}]);

manage.controller('DeleteCtrl', ['$scope', '$state', 'Session', 'MypalApi', function ($scope, $state, Session, MypalApi) {

    /* Delete View Vars */
    $scope.getSelected  = {};
    $scope.getSelected  = Session.get('delete');
    $scope.adID         = $scope.getSelected.id;

    /* Delete Data Functions */
    $scope.runDelete = function () {
        $scope.useBackend(-2, 'deleteAd', function () { return MypalApi.doDelete('pet/' + $scope.adID, null ); });
    };

    /* cancel the form load */
    $scope.cancel = function (event) {
        event.preventDefault();
        $state.go('list');
    };
}]);

/* This filter used in content to convert newlines to breaks */
manage.filter('newLines', function ($sce) {
    return function(text) {
        if (!text) return text;
        text = text.replace(/\r?\n/g, '<br/>');
        return $sce.trustAsHtml(text);
    };
});