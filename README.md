# MyPal Pet

RESTful web service project by Gilbert Rehling.

All REST functions work as expected.

ngDialog is used to load the single item view (More button click in List view).

Search or filter the list view by typing into the 'Search' input field.

Full public access to all functions. (no authentication implemented)

http://mypalpet.gilbert-rehling.com

Copyright (c) 2017 Gilbert Rehling (www.gilbert-rehling.com)