<?php
/**
 * MyPal Pet (mypalpet.gilbert-rehling.com)
 * @version    $Id: $file.filename 10001 18/07/17 9:40 PM Gilbert Rehling $
 * @package    MyPal Pet REST Project by Gilbert Rehling (www.gilbert-rehling.com)
 * @subpackage mypalpet
 * @link       https://mypalpet.gilbert-rehling.com
 * @copyright  Copyright (c) 2017. Gilbert Rehling of MMFAW. All rights reserved. (www.mfmaw.com)
 * @license   MyPal Pet! is licensed software. No unauthorised copying or replication allowed!
 * MyPal Pet may be released as FREEWARE with a limited usage license.
 * This program is designed as a Laravel / Angular project
 * Any other use is prohibited and may infringe on the copyright and usage expectations of ths software.
 * See COPYRIGHT.php for copyright notices and further details.
 */

namespace App\Http\Middleware;

use Closure;

class Api
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // some Server PHP configurations alter the header functions availbility 
        if (!is_callable('getallheaders')) {
            # Convert a string to mixed-case on word boundaries.
            function uc_all($string) {
                $temp = preg_split('/(\W)/', str_replace("_", "-", $string), -1, PREG_SPLIT_DELIM_CAPTURE);
                foreach ($temp as $key=>$word) {
                    $temp[$key] = ucfirst(strtolower($word));
                }
                return join ('', $temp);
            }
        
            function getallheaders() {
                $headers = array();
                foreach ($_SERVER as $h => $v)
                    if (preg_match('/HTTP_(.+)/', $h, $hp))
                        $headers[str_replace("_", "-", uc_all($hp[1]))] = $v;
                return $headers;
            }
        
            function apache_request_headers() { return getallheaders(); }
        }
        
        // get the headers for authentication
        $headers = apache_request_headers();

        // iterate $headers to find 'Authorization-X'
        foreach ($headers as $header => $value) {
            if ($header == "Authorization-X") {
                $value = str_replace("PublicSecret ","", $value); // strip this leading string and space and break
                break;
            }
        }
        $arr    = explode(",",$value);
        $key    = isset($arr[0]) ? $arr[0] : null;
        $secret = isset($arr[1]) ? $arr[1] : null;

        // check length of strings and then test the hard-coded vars below - for production we would fetch the pair from the users table using the supplied UID
        if (strlen($key) == 8 AND strlen($secret) == 17) {
            /* !! For this test we'll use Hard Coded Key and Secret !! */
            $publicKey    = "a1b2c3d4";
            $publicSecret = "z0y9a1b2c3d4x8w7k";
            if ( $publicKey == $key AND $publicSecret == $secret ) {
                // the Public key & secret match those supplied in the HTTP Header (Authorization-X)\
                // return success
                return $next($request);
            }
        }
        // anything that reaches here will fail
        abort(403, 'Accessdenied');
    }
}
