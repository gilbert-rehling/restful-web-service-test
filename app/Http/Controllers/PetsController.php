<?php
/**
 * MyPal Pet (mypalpet.gilbert-rehling.com)
 * @version    $Id: $file.filename 10001 18/07/17 9:40 PM Gilbert Rehling $
 * @package    MyPal Pet REST Project by Gilbert Rehling (www.gilbert-rehling.com)
 * @subpackage mypalpet
 * @link       https://mypalpet.gilbert-rehling.com
 * @copyright  Copyright (c) 2017. Gilbert Rehling of MMFAW. All rights reserved. (www.mfmaw.com)
 * @license   MyPal Pet! is licensed software. No unauthorised copying or replication allowed!
 * MyPal Pet may be released as FREEWARE with a limited usage license.
 * This program is designed as a Laravel / Angular project
 * Any other use is prohibited and may infringe on the copyright and usage expectations of ths software.
 * See COPYRIGHT.php for copyright notices and further details.
 */

namespace App\Http\Controllers;

use DB;
use Input;
use Illuminate\Http\Request;

class PetsController extends Controller
{
    /**
     * Used in the stringReplace function
     *
     * @var array $out Used in the stringReplace function
     */
    private $out = array(",","'");

    /**
     * Used in the stringReplace function
     *
     * @var array $in Used in the stringReplace function
     */
    private $in = array("&#44;","&#39;");

    /**
     * String Replace Method for convenience
     * We can swap the IN's and OUT's when calling this method
     * Laravel dies not like saving single quotes
     *
     * @param  array  $out
     * @param  array  $in
     * @param  string $var
     * @return string
     */
    private function stringReplace($out, $in, $var) {
        return str_replace($out, $in, $var);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = "SELECT * FROM `pets`
                    ORDER BY `id` DESC
                    LIMIT 500";
        $results = DB::select( $query );

        if ($results) {
            // replace any of these which were removed upon insert
            $out    = array("&#44;","&#39;");
            $in     = array(",","'");

            // array to send as JSON the output
            $output = array();
            foreach ($results as $row) {
                $created    = date("d-m-Y h:i:s", strtotime($row->created_at));
                $listDate   = date("d-m-Y", strtotime($row->list_date));
                $saleDate   = isset($row->sale_date) ? date("d-m-Y", strtotime($row->sale_date)) : '';
                $arr = array(
                    'id'              => $row->id,
                    'breed'           => $this->stringReplace($out, $in, $row->breed),
                    'name'            => $this->stringReplace($out, $in, $row->name),
                    'age'             => $row->age,
                    'price'           => $row->price,
                    'listDate'        => $listDate,
                    'saleDate'        => $saleDate,
                    'created'         => $created,
                    'updated'         => $row->updated_at
                );
                $output[] = $arr;
            }
            // return data
            return response()->json(array('records' => $output));
        }
        else
            return response()->json(array('error' => array(array('type' => 'data'), array('message' => 'data incomplete'))));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * Default values will be added for all elements
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $POST = $this->getPostData();

        if ($POST) {
            $breed    = isset($POST['breed'])    && !empty($POST['breed'])    ? $this->stringReplace($this->out, $this->in, $POST['breed']) : 'Unknown';
            $age      = isset($POST['age'])      && !empty($POST['age'])      ? $POST['age']                                                : 1;
            $name     = isset($POST['name'])     && !empty($POST['name'])     ? $this->stringReplace($this->out, $this->in, $POST['name'])  : 'Anonymous';
            $price    = isset($POST['price'])    && !empty($POST['price'])    ? $POST['price']                                              : 0;

            // convert the dates to MySQL datetime format
            $listDate = isset($POST['listDate']) && !empty($POST['listDate']) ? date("Y:m:d h:i:s", strtotime($POST['listDate']))           : null;
            $saleDate = isset($POST['saleDate']) && !empty($POST['saleDate']) ? date("Y:m:d h:i:s", strtotime($POST['saleDate']))           : null;


            // created at reference
            $now = date("Y:m:d h:i:s", time());

            if (strlen($breed) AND strlen($age) AND strlen($price) AND strlen($listDate)) {

                $insertID = DB::table('pets')->insertGetId(
                    ['breed' => $breed, 'age' => $age, 'name' => $name, 'price' => $price, 'list_date' => $listDate, 'sale_date' => $saleDate, 'created_at' => $now]);

                if ($insertID >= 1) {
                    // as the front-end only reloads to the 'list-page' we don't need to send back the whole pet record - just a confirmation
                    $response = array('record' => array( array('success' => '1'), array('id' => $insertID)));
                }
                else
                    $response = array('error' => array( array('type' => 'database'), array('message' => 'insert id not created')));
            }
            else
                $response = array('error' => array( array('type' => 'data'), array('message' => 'required missing')));
        }
        else
            $response = array('error' => array( array('type' => 'post'), array('message' => 'not found')));

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($id) {
            $query = "SELECT * FROM `pets`
              WHERE `id` = ?";
            $results = DB::select($query, [$id]);

            if ($results) {
                // replace any of these which were removed upon insert
                $out = array("&#44;", "&#39;");
                $in = array(",", "'");

                // array to send as JSON the output
                $output = array();
                foreach ($results as $row) {
                    $created = date("d-m-Y h:i:s", strtotime($row->created_at));
                    $listDate = date("Y-m-d", strtotime($row->list_date));
                    $saleDate = isset($row->sale_date) ? date("Y-m-d", strtotime($row->sale_date)) : '';
                    $arr = array(
                        'id' => $row->id,
                        'breed' => $this->stringReplace($out, $in, $row->breed),
                        'name' => $this->stringReplace($out, $in, $row->name),
                        'age' => $row->age,
                        'price' => $row->price,
                        'listDate' => $listDate,
                        'saleDate' => $saleDate,
                        'created' => $created,
                        'updated' => $row->updated_at
                    );
                    $output[] = $arr;
                }
                // return data
                return response()->json(array('record' => $output));

            } else
                return response()->json(array('error' => array(array('type' => 'database'), array('message' => 'item not found'))));
        }
        else
            return response()->json(array('error' => array(array('type' => 'data'), array('message' => 'id missing'))));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($id) {

            $POST = $this->getPostData();

            if ($POST) {
                $breed    = isset($POST['breed'])    && !empty($POST['breed'])    ? $this->stringReplace($this->out, $this->in, $POST['breed']) : 'Unknown';
                $age      = isset($POST['age'])      && !empty($POST['age'])      ? $POST['age']                                                : 1;
                $name     = isset($POST['name'])     && !empty($POST['name'])     ? $this->stringReplace($this->out, $this->in, $POST['name'])  : 'Anonymous';
                $price    = isset($POST['price'])    && !empty($POST['price'])    ? $POST['price']                                              : 0;

                // convert the dates to MySQL datetime format
                $listDate = (strpos($POST['listDate'], "-") !== false ? strtotime($POST['listDate']) : (strlen($POST['listDate'] >= 12) ? $POST['listDate'] / 1000 : strlen($POST['listDate']) > 1 ? strtotime($POST['listDate']) : null));
                $listDate = $listDate != null ? date("Y:m:d h:i:s", $listDate) : null;
                $saleDate = (strpos($POST['saleDate'], "-") !== false ? strtotime($POST['saleDate']) : (strlen($POST['saleDate'] >= 12) ? $POST['saleDate'] / 1000 : strlen($POST['saleDate']) > 1 ? strtotime($POST['saleDate']) : null));
                $saleDate = $saleDate != null ? date("Y:m:d h:i:s", $saleDate) : null;


                // updated at reference
                $now = date("Y:m:d h:i:s", time());

                if (strlen($breed) AND strlen($age) AND strlen($price) AND strlen($listDate)) {

                    $upd = DB::table('pets')->where('id', $id)->update(['breed' => $breed, 'age' => $age, 'name' => $name, 'price' => $price, 'list_date' => $listDate, 'sale_date' => $saleDate, 'updated_at' => $now]);

                    if ($upd >= 1) {
                        // as the front-end only reloads to the 'list-page' we don't need to send back the whole pet record - just a confirmation
                        $response = array('record' => array( array('success' => '1'), array('updated' => $upd)));
                    }
                    else
                        $response = array('error' => array( array('type' => 'database'), array('message' => 'insert id not created')));
                }
                else
                    $response = array('error' => array( array('type' => 'data'), array('message' => 'required missing')));
            }
            else
                $response = array('error' => array( array('type' => 'post'), array('message' => 'not found')));
        }
        else
            $response = array('error' => array( array('type' => 'id'), array('message' => 'ad id not found')));

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($id) {

            $del = DB::table('pets')->where('id', $id)->delete();

            if ($del >= 1) {
                // as the front-end only reloads to the 'list-page' we don't need to send back the whole pet record - just a confirmation
                $response = array('record' => array( array('success' => '1'), array('id' => $id)));
            }
            else
                $response = array('error' => array( array('type' => 'database'), array('message' => 'add not deleted')));
        }
        else
            $response = array('error' => array( array('type' => 'id'), array('message' => 'ad id not found')));

        return response()->json($response);
    }

    /**
     * Global function to decode and sanitise all JSON post data.
     *
     * The code in Angular front-end is using JSON.stringify() before sending via AJAX.
     * In the earlier version of Laravel $post->json() was not returning an array. This custom method
     * cleans up the returned data and sanitises it.
     * In part it was done to potentially enable full control of input data checking and because the native object to array converters seemed to have issues
     *
     * @return array
     */
    private function getPostData()
    {

        $post = new Request();
        $req = $post->json();

        // if the post arrives as an object we need to do some pre-processing
        if (is_object($req) AND count($req) == 1) {

            // this peels the arrayed object out of the JSON - unable to use $req[0]
            foreach ($req as $arr) {
                $new = $arr;
            }

            // strips the curly braces
            $clr = array("{", "}");

            // the only problem here is 'commas' (,) in the data - we have to use data.replace(",","&#44;") in the front-end
            $data = explode(",", str_replace($clr, "", trim($new, "\"")));
            $input = array();

            // finishes converting the JSON object into an array
            foreach ($data as $d) {
                $arr = explode(":", $d);
                if (isset($arr[0], $arr[1])) {
                    $input[trim($arr[0], "\"")] = trim(str_replace("'", "&#39;", $arr[1]), "\"");
                }
            }

        } else {
            $input = $req;
        }

        $arr = array();
        // a bit of sanitising for sanity sake
        foreach ($input as $key => $value) {
            if (!is_array($value)) {
                $val = trim($value);
            } else {
                $val = $value;
            }
            // these filters ensure that the incoming data has been checked
            $val = filter_var($val, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
            $val = filter_var($val, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
            $arr[$key] = $val;
        }
        return $arr;
    }
}
